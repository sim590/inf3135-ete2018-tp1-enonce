# Travail pratique 1 : Génération d'une scène ASCII

Ce dépôt contient l'énoncé du travail pratique 1 du cours INF3135 Construction
et maintenance de logiciels, enseigné par Alexandre Blondin Massé, département
d'informatique, Université du Québec à Montréal, au trimestre d'été 2018.

Le travail doit être remis au plus tard le **8 juin à 23h59**. À partir de
minuit, une pénalité de **2 points** par heure de retard sera appliquée.

## Objectifs pédagogiques

Les principaux objectifs pédagogiques visés sont les suivants :

- Vous initier au langage de **programmation C**.
- Vous initier à l'utilisation du logiciel de contrôle de versions (**Git**)
  lors d'un développement en **solitaire**.
- Vous initier à la compilation d'un programme à l'aide d'un **Makefile**
  simple.
- Vous habituer à décomposer un programme en **petites fonctions**.
- Vous familiariser avec le passage d'arguments par **adresse**.
- Vous familiariser avec l'utilisation de l'**entrée standard** (`stdin`) et de
  la **sortie standard** (`stdout`).

## Description du travail

Vous devez concevoir un programme C nommé `tp1.c` qui permet d'afficher sur la
sortie standard (`stdout`) une scène 2D en format ASCII très simple à partir
d'informations lues sur l'entrée standard (`stdin`).

Votre programme devra avoir un comportement très précis afin d'automatiser la
correction, mais également afin d'automatiser les tests! Vous devrez donc vous
assurer de ne pas écrire de messages superflus sur `stdout` et de bien écrire
**tels quels** les messages d'erreurs s'il y a lieu. En particulier, toutes les
informations d'entrée doivent être lues sur l'**entrée standard** (`stdin`).

Plus précisément, il s'agit de générer une scène rectangulaire en format ASCII
d'au plus **20 lignes** et d'au plus **40 colonnes**. Par exemple, supposez
qu'un fichier texte nommé `exemple.scn` (j'utilise l'extension `scn`, qui
signifie "scène", mais il ne s'agit pas d'un format standard) contienne les
lignes suivantes:
```sh
12 28
r 3 5 2 5 H
+ 1 2 R
d 8 18 3 W
```
Alors on s'attend à ce que la commande
```sh
./tp1 < exemple.scn
```
qui redirige le contenu du fichier `exemple.scn` sur l'entrée standard
(`stdin`) produise le résultat suivant sur la sortie standard (`stdout`):
```sh

..R.........................
RRRRRRRRRRRRRRRRRRRRRRRRRRRR
..R.........................
..R..HHHHH..................
..R..HHHHH..................
..R...............W.........
..R.............WWWWW.......
..R.............WWWWW.......
..R............WWWWWWW......
..R.............WWWWW.......
..R.............WWWWW.......
..R...............W.........
```

Regardons en détails l'information contenue dans le fichier `exemple.scn`:

- Le caractère `.` indique une cellule **non occupée**.
- La ligne `12 28` indique que la scène a `12` **lignes** et `28` **colonnes**.
- La ligne `r 3 5 2 5 H` décrit un **rectangle** (identifié par le caractère
  `r`). Ce rectangle a son **coin supérieur gauche** situé à l'intersection de
  la ligne `3` et de la colonne `5` (les indices commencent à `0`). Sa
  **hauteur** est `2` et sa largeur est `5`. Les **objets** qui occupent ce
  rectangle sont identifiés par le caractère `'H'`. Dans l'exemple, j'ai
  utilisé le caractère `'H'` pour *house*, mais il pourrait s'agir de n'importe
  quel autre caractère affichable.
- La ligne `+ 1 2 R` décrit une **croix**, c'est-à-dire que la ligne `1` est
  complètement occupée par la lettre `'R'`, de même que la colonne `2`. Dans
  l'exemple, j'ai utilisé le caractère `'R'` pour *road*.
- La ligne `d 8 18 3 W` décrit un **disque** occupé par le caractère `W`, dont
  le centre est situé à l'intersection de la ligne `8` et de la colonne `18`,
  et dont le rayon est `3`. Notez qu'une case `(i,j)` se trouve dans le disque
  de centre `(h,k)` et de rayon `r` si et seulement si la condition
  `(i - h)^2 + (j - k)^2 <= r^2` est vérifiée, où le caractère `^` dénote
  l'exponentiation. Dans l'exemple, j'ai utilisé le caractère `'W'` pour
  *water*.

## Gestion des erreurs

Votre programme doit vérifier si les entrées fournies sont valides,
c'est-à-dire que leur format respecte bien la spécification:

- La première ligne doit être de la forme `<num_lignes> <num_colonnes>`.
- Les lignes qui suivent doivent être d'une des trois formes suivantes:

  * `r <i> <j> <h> <w> <c>`, où `<i>` et `<j>` indiquent la ligne et la colonne
    auxquelles se trouve le coin supérieur gauche du rectangle, et `<h>` et
    `<w>` indiquent la hauteur et la largeur du rectangle. À noter que `<i>` et
    `<j>` peuvent être des entiers négatifs, mais pas `<h>` ni `<w>`. Le
    paramètre `<c>` est le caractère utilisé pour "remplir" le rectangle.
  * `+ <i> <j> <c>`, où `<i>` et `<j>` indiquent les indices de la ligne et de
    la colonne qui déterminent la croix et `<c>` est le caractère utilisé.
  * `d <i> <j> <r> <c>`, où `<i>` et `<j>` indiquent les indices de la ligne et
    de la colonne où se situe le centre du disque, `<r>` est le "rayon" du
    disque et `<c>` le caractère à utiliser pour "remplir" le disque. Noter que
    le paramètre `<r>` doit être positif, sinon un message d'erreur doit être
    affiché.

**Remarque**: Dans les trois cas, les indices `<i>` et `<j>` peuvent être
positifs ou négatifs et même se trouver à l'extérieur de la scène. Par exemple,
si on ajoute un objet complètement extérieur à la scène, il n'y aura pas
d'erreur, mais la scène restera inchangée. Si on ajoute un objet dont le centre
est à l'extérieur, mais qu'il intersecte une partie de la scène, alors il faut
que la partie *visible* y apparaisse.

En cas de non respect de ces contraintes, un message très précis doit être
affiché sur `stdout` et la fonction `main` doit retourner un code d'erreur
spécifique:

S'il n'y a pas d'erreur, le programme retourne `0`.

Si la première ligne n'est pas de la bonne forme pour les dimensions, le
message
```sh
Error: The two first values must indicate the dimensions of the scene.
```
doit être affiché. Si le nombre de lignes spécifiées n'est pas entre 1 et 20,
alors vous devez afficher le message d'erreur
```sh
Error: The number of lines must be between 1 and 20.
```
et si le nombre de colonnes n'est pas entre 1 et 40, on devrait plutôt
```sh
Error: The number of columns must be between 1 and 40.
```
Dans les trois cas, le code d'erreur `1` doit être retourné pour indiquer que
les dimensions sont invalides.

Si le type d'objets indiqué n'est pas `r`, `+` ou `d`, alors le message
d'erreur
```sh
Error: Unrecognized object type
```
doit être affiché. Ensuite, plus spécifiquement, s'il y a un problème avec un
rectangle, on doit lire
```sh
Error: invalid rectangle object.
Expected: r <i> <j> <h> <w> <c>, where
  <i> is the line number
  <j> is the column number
  <h> is the height of the rectangle
  <w> is the width of the rectangle
  <c> is a char denoting the object.
```
Pour les croix problématiques, le message devrait plutôt être
```sh
Error: invalid '+' object.
Expected: + <i> <j> <c>, where
  <i> is the line number of the crossing
  <j> is the column number of the crossing
  <c> is a char denoting the object.
```
alors que pour les disques, c'est
```sh
Error: invalid disk object.
Expected: d <i> <j> <r> <c>, where
  <i> is the line number of the center of the disk
  <j> is the column number of the center of the disk
  <r> is the radius of the disk
  <c> is a char denoting the object.
```
Dans le cas d'objets invalides, votre programme doit retourner le code d'erreur
`2`.

Je vous encourage à étudier le contenu du fichier `test.bats` pour avoir plus
de détails sur les différents scénarios possibles d'erreur.

## Marche à suivre

Afin de compléter ce travail pratique, vous devrez suivre les étapes suivantes
(pas nécessairement dans l'ordre):

1. Clonez (mais sans faire de *fork*) le [dépôt du
   projet](https://gitlab.com/ablondin/inf3135-ete2018-tp1).
2. Créez un dépôt nommé `inf3135-ete2018-tp1`.
3. Donnez accès à votre dépôt à l'utilisateur `ablondin` en mode `Developer`.
4. Créez un fichier `tp1.c` avec une coquille de base qui compile.
5. Ajoutez un fichier `Makefile` pour automatiser la compilation.
6. Commencez votre développement en versionnant fréquemment l'évolution de
   votre projet avec Git. La qualité de votre versionnement sera évaluée, il
   est donc important de valider (*commit*) chaque fois que vous avez terminé
   une petite tâche.
7. Complétez le fichier `Makefile` pour qu'il exécute toutes les tâches
   demandées.
8. Complétez le fichier `README.md` en respectant le format Markdown.

### Clone et création du dépôt

Vous devez cloner le dépôt fourni et l'héberger sur la plateforme
[Gitlab](https://gitlab.com/). Ne faites pas de *fork*, car cela pourra
entraîner certains problèmes (nous verrons les *forks* dans le TP2). Votre
dépôt devra se nommer **exactement** `inf3135-ete2018-tp1` et l'URL devra être
**exactement** `https://gitlab.com/<utilisateur>/inf3135-ete2018-tp1`, où
`<utilisateur>` doit être remplacé par votre identifiant GitLab. Il devra être
**privé** et accessible seulement par vous et par l'utilisateur `ablondin`.

### Programme `tp1.c`

Votre programme doit être écrit dans un seul fichier nommé `tp1.c`, placé dans
la racine du projet.  Le programme doit être écrit en C, compatible avec C11 et
compilable avec `gcc` version 4.8 ou ultérieure. Assurez-vous qu'il fonctionne
correctement sur les serveurs Malt ou Java.

Voici à titre indicatif certains types et prototypes (il peut en manquer, je ne
vous ai pas tout donné) que j'utilise dans ma solution.  Vous pouvez les
utiliser tels quels, ne pas les utiliser du tout ou les modifier pour les
adapter à votre façon de modéliser le problème. Évidemment, n'oubliez pas de
les documenter selon le standard Javadoc.
```c
// --------- //
// Constants //
// --------- //

#define MAX_NUM_LINES    20
#define MAX_NUM_COLUMNS  40

// ----- //
// Types //
// ----- //

struct Scene {
    unsigned int num_lines;
    unsigned int num_columns;
    char objects[MAX_NUM_LINES][MAX_NUM_COLUMNS];
};

enum Status {
    OK                     = 0,
    ERR_INVALID_DIMENSIONS = 1,
    ERR_INVALID_OBJECT     = 2,
};

// ---------- //
// Prototypes //
// ---------- //

void initialize_scene(struct Scene *scene,
                      unsigned int l,
                      unsigned int c);
void print_scene(const struct Scene *scene);
void add_rectangle(struct Scene *scene,
                   unsigned int i,
                   unsigned int j,
                   unsigned int h,
                   unsigned int w,
                   char object);
void add_plus(struct Scene *scene,
              unsigned int i,
              unsigned int j,
              char object);
void add_disk(struct Scene *scene,
              unsigned int i,
              unsigned int j,
              unsigned int r,
              char object);
enum Status load_object(struct Scene *scene, char object_type);
enum Status load_scene(struct Scene *scene);
```

### Makefile

Vous devrez supporter les comportements suivants dans votre Makefile:

- La commande `make` crée l'exécutable `tp1` s'il y a eu une modification du
  fichier `tp1.c`.
- La commande `make html` génère un fichier `README.html` à partir du fichier
  `README.md` à l'aide du logiciel [Pandoc](https://pandoc.org/).
- La commande `make check` lance la suite de tests contenue dans le fichier
  `test.bats` **non modifié**, livré dans le dépôt que vous avez cloné.
- La commande `make clean` supprime les fichiers inutiles (`.o`, `.html`,
  `.swp`, etc.).

N'oubliez pas les **dépendances** pour chaque cible s'il y en a!

### Fichier `README.md`

Vous devez compléter le fichier `README.md` livré dans le dépôt en suivant les
instructions qui y sont indiquées. Assurez-vous également de répondre aux
questions suivantes:

* À quoi sert votre programme?
* Comment le compiler?
* Comment l'exécuter?
* Quels sont les formats d'entrées et de sorties?
* Quels sont les cas d'erreur gérés?

Vous devez utiliser la syntaxe Markdown pour écrire une documentation claire et
lisible. Vous pouvez en tout temps vérifier localement le fichier HTML produit
sur votre machine à l'aide de Pandoc, mais il est aussi conseillé de vérifier,
avant la remise finale, que le résultat produit sur la page d'accueil de GitLab
est celui auquel vous vous attendez.

Évitez d'abuser du gras et de l'italique dans le fichier `README.md`. Exploitez
au maximum les listes à puces et formatez les noms de fichier et le code à
l'aide des apostrophes inversés. Finalement, soignez la qualité de votre
français, qui sera évaluée plus particulièrement dans le fichier `README.md`.

### Git

Les sources de votre programme devront être versionnées à l'aide de Git.  Vous
devez cloner le gabarit du projet fourni et ajouter vos modifications à l'aide
de *commits*.

Il est très, très important de conserver l'**historique** du dépôt, incluant
les modifications que j'y ai apportées. En particulier, il est possible que je
doive apporter des corrections ultérieures à l'énoncé que vous pourrez
récupérer beaucoup plus facilement si nous avons un historique **commun**.

Finalement, n'oubliez pas d'inclure un fichier `.gitignore` en fonction de
votre environnement de développement. Aussi, assurez-vous de ne pas versionner
de fichiers inutiles!

Adresse du dépôt à cloner:
[https://gitlab.com/ablondin/inf3135-ete2018-tp1](https://gitlab.com/ablondin/inf3135-ete2018-tp1).

## Correction

L'exécution de votre programme sera vérifiée automatiquement grâce à une suite
de tests rédigée en [Bats](https://github.com/sstephenson/bats). Notez que vous
n'avez pas à connaître la syntaxe de ce logiciel pour pouvoir l'utiliser, mais
n'hésitez pas à aller y jeter un coup d'oeil puisqu'il sera étudié plus en
profondeur un peu plus tard dans le cours.

Pour faciliter votre développement, vous avez accès à un certain nombre de
tests *publics* pour corriger vos travaux (dans le fichier `test.bats`). Il
suffit d'entrer la commande
```sh
bats test.bats
```
pour lancer la suite de tests.

Cependant, il n'y a aucune garantie que la couverture de tests est
**complète**. Je me réserve donc la possibilité d'ajouter des tests
supplémentaires pour la correction finale (généralement, je n'en ajoute que
quelques-uns). Par conséquent, si vous avez un doute sur le comportement
attendu d'une certaine situation, je vous invite à me poser des questions pour
que je puisse apporter des précisions à l'ensemble de la classe si nécessaire.

### Barème

Les critères d'évaluation sont les suivants:

| Critère            | Points |
| -------            | -----: |
| Fonctionnabilité   | /40    |
| Qualité du code    | /20    |
| Documentation      | /15    |
| Makefile           | /10    |
| Utilisation de Git | /15    |
| Total              | /100   |

Les critères plus détaillés sont décrits ci-bas:

* **Fonctionnabilité (40 points)**: Le programme passe les tests *publics* et
  *privés* en affichant le résultat attendu.

* **Qualité du code (20 points)**: Les identifiants utilisés sont significatifs
  et ont une syntaxe uniforme (`camelCase` ou `snake_case`), le code est bien
  indenté, il y a de l'aération autour des opérateurs et des parenthèses, le
  programme est simple et lisible.  Pas de bout de code en commentaire ou de
  commentaires inutiles. Pas de valeur magique. Le code doit être bien
  factorisé (pas de redondance). Il est décomposé en petites fonctions qui
  effectuent des tâches spécifiques. La présentation est soignée de façon
  générale. *Note*: Si votre travail est peu avancé, vous n'aurez aucun point
  pour cette partie.

* **Documentation (15 points)**: Le fichier `README.md` est complet et respecte
  le format Markdown. L'en-tête du fichier `tp1.c` est bien documentée, de même
  que chacune des fonctions (*docstrings*) en suivant le standard Javadoc.

* **Makefile (10 points)**: Le Makefile supporte les appels `make`, `make
  html`, `make check` et `make clean`. Toutes les dépendances de ces cibles
  sont présentes.

* **Utilisation de Git (15 points)**: Les modifications sont réparties en
  plusieurs *commits*. Le fichier `.gitignore` a été mis à jour. Les messages
  de *commit* sont significatifs et respectent le format demandé (première
  ligne courte de la forme `<sujet>: <courte description>`, suivi d'une ligne
  vide et de paragraphes détaillés si nécessaires).

### Pénalités

Tout programme qui ne compile pas se verra automatiquement attribuer **la note
0**.

En outre, si vous ne respectez pas les critères suivants, une pénalité de
**50%** sera imposée :

- Votre dépôt doit se nommer **exactement** `inf3135-ete2018-tp1` sur GitLab;

- L'URL de votre dépôt doit être **exactement**
  `https://gitlab.com/<utilisateur>/inf3135-ete2018-tp1` où `<utilisateur>`
  doit être remplacé par votre identifiant GitLab.

- L'utilisateur `ablondin` doit avoir accès à votre projet en mode *Developer*.

- Votre dépôt est un **clone** du [gabarit
  fourni](https://gitlab.com/ablondin/inf3135-ete2018-tp1).

- Votre dépôt doit être **privé**.

## Remise

La remise se fait automatiquement en ajoutant l'utilisateur `ablondin` en mode
*Developer*, vous n'avez rien de plus à faire. Je corrigerai simplement la
dernière validation (*commit*) disponible sur votre branche `master`.

Le travail doit être remis au plus tard le **8 juin à 23h59**. À partir de
minuit, une pénalité de **2 points** par heure de retard sera appliquée.
